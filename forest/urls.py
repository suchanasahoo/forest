"""forest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from api import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index),
    # path('/', views.index),
    path('admin/', admin.site.urls),
    url(r'^(?P<category_id>\d+)/$',views.garden_category, name="garden_category"),
    path('contact/',views.contact, name="contact"),
    url(r'^garden_single/(?P<post_id>\d+)/$',views.garden_single, name='garden_single'),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


