from django import forms
from api.models import contact, Feedback

class ContactForm(forms.Form):
    name = forms.CharField(label='Name')
    email = forms.EmailField(label='Email')
    ph = forms.CharField(label='Ph')
    subject = forms.CharField(label='Subject')
    message = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

class ContactDataForm(forms.ModelForm):
    class Meta:
        model = contact
        fields = ('name', 'email', 'ph', 'subject', 'message')
    
class FeedbackForm(forms.Form):
    name = forms.CharField(max_length=50)
    email = forms.EmailField()
    comment = forms.CharField(widget=forms.Textarea)
    
    def __init__(self, *args, **kwargs):
        super(FeedbackForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

class FeedbackDataForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ('name', 'email', 'comment')
