from django.db import models
# from django.contrib.auth.models import User

# Create your models here.

class category(models.Model):
    cat_name = models.CharField(max_length=200)

    def __str__(self):
        return self.cat_name

class post(models.Model):
    category = models.ForeignKey(category, on_delete=models.CASCADE)
    heading = models.CharField(max_length=150)
    image = models.ImageField(upload_to='post', default='')
    description = models.CharField(max_length=500)
    date = models.DateField()
    editor = models.CharField(max_length=50)

    def __str__(self):
        return '{}{}{}{}{}{}'.format(self.category, self.heading, self.image, self.description, self.date, self.editor)

class contact(models.Model):
    name = models.CharField(max_length=150)
    email = models.EmailField()
    ph = models.CharField(max_length=10)
    subject = models.CharField(max_length=100)
    message = models.CharField(max_length=500)

    def __str__(self):
        return '{}{}{}{}{}'.format(self.name, self.email, self.ph, self.subject, self.message)

class Feedback(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    comment = models.TextField()
    post = models.ForeignKey(post,on_delete=models.CASCADE)

    def __str__(self):
        return '{}{}{}'.format(self.name,self.email,self.comment)
