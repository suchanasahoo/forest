from django.shortcuts import render
from django.http import HttpResponse
from api.models import category, post, Feedback
from api.forms import ContactForm, ContactDataForm, FeedbackForm, FeedbackDataForm

# Create your views here.

def index(request):
    category_list = category.objects.all()
    post_list = post.objects.all()

    return render(request,'garden-index.html',{'category_list':category_list,'post_list':post_list})

def garden_category(request, category_id):
    post_data = post.objects.filter(category_id=category_id)

    return render(request, "garden-category.html", {'post_data':post_data})

def garden_single(request,post_id):
    
    feedbackObject = FeedbackForm()
    if request.method == 'POST':
        form = FeedbackDataForm(request.POST)
        obj = Feedback()

        if form.is_valid():
            obj.name = form.cleaned_data['name']
            obj.email = form.cleaned_data['email']
            obj.comment = form.cleaned_data['comment']
            obj.post_id = post_id
            obj.save()
    
    post_data = post.objects.get(id=post_id)
    feedback_data = Feedback.objects.filter(post_id=post_id)

    garden_single_data = {'post_data':post_data, 'feedback_data':feedback_data, 'form':feedbackObject}
    return render(request, "garden-single.html", garden_single_data)

def contact(request):
    contactFormObject = ContactForm()
    if request.method == 'POST':
        form = ContactDataForm(request.POST)
        if form.is_valid():
            form.save()
        
    return render(request,'garden-contact.html',{'form':contactFormObject})



    
