from django.contrib import admin
from api.models import category,post,contact,Feedback

# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id','cat_name']

class PostAdmin(admin.ModelAdmin):
    list_display = ['id','heading','image','description','date','editor','category']

class ContactAdmin(admin.ModelAdmin):
    list_display = ['id','name','email','ph','subject','message']

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ['id','name','email','comment','post_id']

admin.site.register(category,CategoryAdmin)
admin.site.register(post,PostAdmin)
admin.site.register(contact,ContactAdmin)
admin.site.register(Feedback,FeedbackAdmin)